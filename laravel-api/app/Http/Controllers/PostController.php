<?php

namespace App\Http\Controllers;

use App\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class PostController extends Controller
{
    public function __construct()
    {
        return $this->middleware('auth:api')->only(['store' , 'update' , 'delete']);
    }

    public function index()
    {
        $posts = Post::latest()->get();

        return response()->json([
            'success' => true,
            'message' => 'Data daftar post berhasil ditampilkan',
            'data'    => $posts
        ]);
    }

    public function store(Request $request)
    {
        $allRequest = $request->all();
        
        $validator = Validator::make($allRequest , [
            'title' => 'required',
            'description' => 'required'
        ]);

        if($validator->fails()){
            return response()->json($validator->errors() , 400);
        }

        $post = Post::create([
            'title' =>  $request->title,
            'description' => $request->description,
        ]);

        if($post){
            return response()->json([
                'success'   => true,
                'message'   => 'Data Post berhasil dibuat',
                'data'      =>  $post
            ], 200);
        }

        return response()->json([
            'success'   => false,
            'message'   => 'Data Post gagal dibuat'
        ], 409);

    }  

    public function show($id)
    {
        $post = Post::find($id);

        if($post)
        {
            return response()->json([
                'success' => true,
                'message' => 'Data post berhasil ditampilkan',
                'data'    => $post
            ], 200);
        }

        return response()->json([
            'success' => false,
            'message' => 'Data dengan id : ' .  $id . '  tidak ditemukan',
        ], 404);


    }

    public function update(Request $request , $id)
    {
        $allRequest = $request->all();

        $validator = Validator::make($allRequest, [
            'title' => 'required',
            'description' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $post = Post::find($id);

        if($post)
        {
            $user = auth()->user();

            if($post->user_id != $user->id)
            {
                return response()->json([
                    'success' => false,
                    'message' => 'Data post bukan milik user login',
                ] , 403);

            }

            $post->update([
                'title' => $request->title,
                'description' => $request->description,
            ]);

            return response()->json([
                'success' => true,
                'message' => 'Data dengan judul : ' . $post->title . '  berhasil diupdate',
                'data' =>    $post
            ]);
        }

        return response()->json([
            'success' => false,
            'message' => 'Data dengan id : '. $id .' tidak ditemukan',
        ], 404);
    }

    public function destroy($id)
    {
        $post = Post::find($id);

        if ($post) {
            $user = auth()->user();

            if ($post->user_id != $user->id) {
                return response()->json([
                    'success' => false,
                    'message' => 'Data post bukan milik user login',
                ], 403);
            }

            $post->delete();

            return response()->json([
                'success' => true,
                'message' => 'Data post berhasil dihapus',
            ], 200);
        }

        return response()->json([
            'success' => false,
            'message' => 'Data dengan id : ' .  $id . '  tidak ditemukan',
        ], 404);
    }
}
