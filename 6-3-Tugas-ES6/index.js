//Jawaban Soal Nomor 1
let luasPersegipanjang = function (panjang, lebar) {
  return panjang * lebar;
};
console.log('Luas Persegi Panjang =', luasPersegipanjang(6, 8));

let kelilingPersegipanjang = function (panjang1, lebar1) {
  return 2 * (panjang1 * lebar1);
};
console.log('Keliling Persegi Panjang =', kelilingPersegipanjang(9, 5));

//Jawaban Soal Nomor 2
let newFunction = (firstName, lastName) => {
  return `${firstName} ${lastName}`;
};

//Driver Code
newFunction('William', 'Imoh');

//Jawaban Soal Nomor 3
const newObject = {
  firstName: 'Muhammad',
  lastName: 'Iqbal Mubarok',
  address: 'Jalan Ranamanyar',
  hobby: 'playing football',
};

//  destructuring
const { firstName, lastName, address, hobby } = newObject;

//  Driver code
console.log(firstName, lastName, address, hobby);

//Jawaban Soal Nomor 4
const west = ['Will', 'Chris', 'Sam', 'Holly'];
const east = ['Gill', 'Brian', 'Noel', 'Maggie'];
const combined = [...west, ...east];
//Driver Code
console.log(combined);

//Jawaban Soal Nomor 5
const planet = 'earth';
const view = 'glass';
const before = `Lorem ${view} dolor sit amet, consectetur adipiscing elit,${planet} `;
console.log(before);
