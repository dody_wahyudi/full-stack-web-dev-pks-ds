// soal 2

const readBooks = require('./callback.js');
var readBooksPromise = require('./promise.js');

var books = [
  { name: 'LOTR', timeSpent: 3000 },
  { name: 'Fidas', timeSpent: 2000 },
  { name: 'Kalkulus', timeSpent: 4000 },
  { name: 'komik', timeSpent: 1000 },
];

//.... jawaban soal 2
// Lanjutkan code untuk menjalankan function readBooksPromise
function showSisaWaktu(sisaWaktu) {
  console.log(`sisa waktu : $(sisaWaktu)`);
}
const maxTime = 3500;

books.forEach(async (book) => {
  try {
    const response = await readBooks(maxTime, book);
    showSisaWaktu(response);
  } catch (error) {
    showSisaWaktu(error);
  }
});
